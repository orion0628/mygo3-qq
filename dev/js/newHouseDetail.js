// 字數控制
$(function () {
    var len = 300; // 超過50個字以"..."取代
    $(".JQellipsis").each(function (i) {
      if ($(this).text().length > len) {
        $(this).attr("title", $(this).text());
        var text = $(this).text().substring(0, len - 1) + "...";
        $(this).text(text);
      }
    });
  });

// ----Swiper JS
// 大banner
var topBannerFluid = new Swiper('#topBanner-fluid', {
    slidesPerView: 1,
    spaceBetween: 30,
    loop: true,
    autoplay: true,
    delay: 1000,
    loopAdditionalSlides: 1,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  });

//建案像本
var houseAlbum1 = new Swiper('#houseAlbum1', {
    slidesPerView: 2.1,
    spaceBetween: 5,
    // init: false,
    delay: 1000,
    loop : false,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    breakpoints: {
      640: {
        slidesPerView: 1.1,
        spaceBetween:5,
      },
      768: {
        slidesPerView: 2.1,
        spaceBetween: 5,
      },
      1024: {
        slidesPerView: 2.1,
        spaceBetween: 5,
      },
    }
  });
  var houseAlbum2 = new Swiper('#houseAlbum2', {
    slidesPerView: 2.1,
    spaceBetween: 5,
    // init: false,
    delay: 1000,
    loop : false,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    breakpoints: {
      640: {
        slidesPerView: 1.1,
        spaceBetween:5,
      },
      768: {
        slidesPerView: 2.1,
        spaceBetween: 5,
      },
      1024: {
        slidesPerView: 2.1,
        spaceBetween: 5,
      },
    }
  });
  var houseAlbum3 = new Swiper('#houseAlbum3', {
    slidesPerView: 2.1,
    spaceBetween: 5,
    // init: false,
    delay: 1000,
    loop : false,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    breakpoints: {
      640: {
        slidesPerView: 1.1,
        spaceBetween:5,
      },
      768: {
        slidesPerView: 2.1,
        spaceBetween: 5,
      },
      1024: {
        slidesPerView: 2.1,
        spaceBetween: 5,
      },
    }
  });
  var houseAlbum4 = new Swiper('#houseAlbum4', {
    slidesPerView: 2.1,
    spaceBetween: 5,
    // init: false,
    delay: 1000,
    loop : false,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    breakpoints: {
      640: {
        slidesPerView: 1.1,
        spaceBetween:5,
      },
      768: {
        slidesPerView: 2.1,
        spaceBetween: 5,
      },
      1024: {
        slidesPerView: 2.1,
        spaceBetween: 5,
      },
    }
  });
  var houseAlbum5 = new Swiper('#houseAlbum5', {
    slidesPerView: 2.1,
    spaceBetween: 5,
    // init: false,
    delay: 1000,
    loop : false,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    breakpoints: {
      640: {
        slidesPerView: 1.1,
        spaceBetween:5,
      },
      768: {
        slidesPerView: 2.1,
        spaceBetween: 5,
      },
      1024: {
        slidesPerView: 2.1,
        spaceBetween: 5,
      },
    }
  });

  //開發商同建案
  var buildingPromo = new Swiper('#buildingPromo', {
    slidesPerView: 3,
    spaceBetween: 10,
    autoplay:true,
      delay: 1000,
      // width:auto,
      // loop : true,
      loopAdditionalSlides : 1,
    // init: false,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    breakpoints: {
      640: {
        slidesPerView: 1.7,
        spaceBetween: 5,
      },
      768: {
        slidesPerView: 3,
        spaceBetween: 5,
      },
      1024: {
        slidesPerView: 3,
        spaceBetween: 4,
      },
    }
  });

  //同區建案
  var buildingPromo2 = new Swiper('#buildingPromo2', {
    slidesPerView: 3,
    spaceBetween: 15,
    autoplay: true,
    delay: 1000,
    loop: true,
    loopAdditionalSlides: 1,
    // init: false,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    breakpoints: {
      640: {
        slidesPerView: 1.7,
        spaceBetween: 5,
      },
      768: {
        slidesPerView: 3,
        spaceBetween: 5,
      },
      1024: {
        slidesPerView: 3,
        spaceBetween: 15,
      },
    }
  });

//   同區推薦建案
  var buildingPromo2 = new Swiper('#buildingPromo2', {
    slidesPerView: 3,
    spaceBetween: 15,
    autoplay: true,
    delay: 1000,
    loop: true,
    loopAdditionalSlides: 1,
    // init: false,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    breakpoints: {
      640: {
        slidesPerView: 1.7,
        spaceBetween: 5,
      },
      768: {
        slidesPerView: 3,
        spaceBetween: 5,
      },
      1024: {
        slidesPerView: 3,
        spaceBetween: 15,
      },
    }
  });

//   海外地產推薦
  var buildingPromo3 = new Swiper('#buildingPromo3', {
    slidesPerView: 4,
    spaceBetween: 15,
    autoplay: true,
    delay: 1000,
    loop: true,
    loopAdditionalSlides: 1,
    // init: false,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    breakpoints: {
      640: {
        slidesPerView: 2.1,
        spaceBetween: 5,
      },
      768: {
        slidesPerView: 3,
        spaceBetween: 5,
      },
      1024: {
        slidesPerView: 4,
        spaceBetween: 15,
      },
    }
  });

// 相本切換的標籤效果
  jQuery(function(){
    jQuery('.targetDiv').hide();
    jQuery('#Album1').show();
         jQuery('#showall').click(function(){
               jQuery('.targetDiv').show();
        });
        jQuery('.showSingle').click(function(){
              jQuery('.targetDiv').hide();
              jQuery('#Album'+$(this).attr('target')).show();
              $(".showSingle").removeClass("active");
              $(this).addClass("active");
        });
});