﻿var mk_header_parallax, mk_responsive_nav_width, mk_header_sticky, mk_header_parallax, mk_banner_parallax, mk_page_parallax, mk_footer_parallax, mk_body_parallax;
  mk_images_dir = "images",
  mk_theme_js_path = "js",
  mk_responsive_nav_width = 1040,
  mk_smooth_scroll = false,
  mk_header_sticky = true;
  mk_header_parallax = false,
  mk_banner_parallax = false,
  mk_page_parallax = false,
  mk_footer_parallax = false,
  mk_body_parallax = false;