  // 一級菜單下拉
  function navlist(){
    var screenWidth = $(window).width();
    if(screenWidth > 960){
      var lis = $('.navbar-nav>li');
      var lisSec = $('.dropdown-menu li');
      lis.hover(function(){
        $(this).siblings().addClass('opacity');
        $(this).find('.dropdown-menu').slideDown(300);
        $(this).siblings().find('.dropdown-menu').slideUp(300).finish();
      },function(){
        lis.removeClass('opacity');
        lis.find('.dropdown-menu').slideUp(300);
      })
    }
  }

  //二级菜单滑动
  function containSlider(){
    var tag = $('.innerslider');
    var full_width = tag.width() - document.documentElement.clientWidth;
    tag.bind('touchstart',function(e){
      var tagL = parseInt(tag.css('left'));
      var l = e.originalEvent.targetTouches[0].pageX;
      var expL = l - tagL;
      move(expL);
    })

    function move(left){
      tag.bind('touchmove',function(e){
        e.preventDefault();
        var l = e.originalEvent.targetTouches[0].pageX;
        $(this).css({left:l-left});
        var miseeLeft = parseInt($(this).css('left'));
        //允许拉动的左右最长距离
        if(miseeLeft > 140){
          $(this).css({left:0});
        }else if(miseeLeft < -167){
          $(this).css({left: '-167px'});
        }
      })
    }

    tag.bind('touchend',function(e){
      var miseeLeft = parseInt($(this).css('left'));
      var lastChild = $('.innerslider li:last');
      var tl = lastChild.position().left + lastChild.innerWidth() - $(window).width();//最后一个li距离左边距的距离
      console.info(miseeLeft,tl)
      if(miseeLeft > 0){
        $(this).animate({left:0},200);
      }else if(miseeLeft < -tl){
        $(this).animate({left:-tl},200);
        console.info(tl+'haha')
      }
      $(this).unbind("touchmove");
      // $(this).unbind("touchend");
    })
  }

  // 回滾頂部

  $('.toTop').on('click',function(){
    $('html,body').animate({scrollTop: '0px'},300)
  })


  // 字数限制
  function textLimit(_this,max){
    var textLength = $(_this).text().length;
    var vals = $(_this).text().substring(0,max);
    if(textLength >= max){
      $(_this).text(vals + '...')
    }
  }

  // 选择语言
  function languageSel(){
    var headLang = $('.header .langCon .showin');
    var langBtn = $('.header .langCon > a');
    var langItem = $('.header .langCon .showin li')
    langBtn.on('click',function(e){
      e.stopPropagation()
      var check = $(this).hasClass('active');
      if(!check){
        headLang.slideDown(300);
        $(this).addClass('active');
      }else{
        headLang.slideUp(300);
        $(this).removeClass('active');
      }
    })

    $(document).on('click',function(e){
      e.stopPropagation();
      headLang.slideUp(300);
      langBtn.removeClass('active');
    })
  }

  // 选择位置
  function positionSel(){
    var headLang = $('.header .addrCon .showin');
    var langBtn = $('.header .addrCon > a');
    var langItem = $('.header .addrCon .showin li')
    langBtn.on('click',function(e){
      e.stopPropagation()
      var check = $(this).hasClass('active');
      if(!check){
        headLang.slideDown(300);
        $(this).addClass('active');
      }else{
        headLang.slideUp(300);
        $(this).removeClass('active');
      }
    })

    $(document).on('click',function(e){
      e.stopPropagation();
      headLang.slideUp(300);
      langBtn.removeClass('active');
    })
  }

  // 我的mygo
  function loginOut() {
    var headLang = $('.header .bg-login .showin');
    var loginBtn = $('.header .bg-login > a');
    loginBtn.on('click',function(e){
      e.stopPropagation()
      var check = $(this).hasClass('active');
      if(!check){
        headLang.slideDown(300);
        $(this).addClass('active');
      }else{
        headLang.slideUp(300);
        $(this).removeClass('active');
      }
    })

    $(document).on('click',function(e){
      e.stopPropagation();
      headLang.slideUp(300);
      loginBtn.removeClass('active');
    })
  }

  // 遮罩
  function shadows(){
    $('.navbar-header button,.search-login>div>span').on('click',function(){
      var check = $(this).hasClass('collapsed');
      var id = $(this).attr('data-target');
      if(check){
        $('.header .slide-btn').not($(this)).addClass('collapsed')
        $('.header .collapse').not(id).removeClass('in')
        $('.shadow').fadeIn(300);
      }else{
        $('.shadow').fadeOut(300);
      }
    })
  }

  // 国内房产海外房产收放
  function fooListShowhide(){
    var contain = $('.fir-floor ._items');
    var btn = $('.fir-floor .fo-title i');
    btn.on('click',function(){
      var check = $(this).hasClass('open');
      if(!check){
        $(this).addClass('open');
        $(this).parent().next().slideUp(300);
      }else{
        $(this).removeClass('open');
        $(this).parent().next().slideDown(300);
      }
    })
  }

  // 右下角选择栏隐藏显示
  function winScroll(){
    var _width = $(window).width();
    var wechat = $('.serverContain ul li.wechat')
    var showLine,tagContain;
    if(_width < 768){
      showLine = 700;
      tagContain = $('.forMobile');
    }else{
      showLine = 1000;
      tagContain = $('.forPC');
    }
    $(window).scroll(function(){
      var _top = $(this).scrollTop();
      if(_top > showLine){
        tagContain.show()
      }else{
        tagContain.hide()
      }
    })

    wechat.on('click',function(){
      var check = $(this).hasClass('active');
      if(!check){
        $(this).find('.codeCon').fadeIn(300);
        $(this).addClass('active')
      }else{
        $(this).find('.codeCon').fadeOut(300);
        $(this).removeClass('active')
      }
      
    })
  }

  $(document).ready(function(){
    languageSel();
    loginOut();
    navlist();
    shadows();
    containSlider();
    fooListShowhide();
    winScroll();
    positionSel();
  })