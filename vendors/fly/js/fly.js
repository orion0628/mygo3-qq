
$(function(){

	$(".btnJoin").on("click", OPENOPEN );
	
	function OPENOPEN(){
		$("#contactJoin").slideDown(300);
		$(".close").on("click", CLOSECLOSE);
	}
	
	function CLOSECLOSE(){
	    $("#contactJoin").slideUp(300);
		$(".close").off("click");
	}
	
	$(window).on("resize",CLEARSTYLE);
	
	function CLEARSTYLE(){
		if($(window).innerWidth()>736){
			$(".NAV").attr("style","");
		}
	}
	
});
